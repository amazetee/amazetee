<?php 

// FOOTER FILE - Footer and social Bar

?>

	<footer>
			<div id="footlink">
				<div style="height: 100px;">
					<div id="sharing">
						<p>Share</p>
						<a href="https://www.facebook.com"><img src="images/social/fb.png" id="fb" class="social"></a>
						<a href="https://www.twitter.com"><img src="images/social/twitter.png" id="twitter" class="social"></a>
						<a href="https://plus.google.com/"><img src="images/social/g+.png" id="google" class="social"></a>
						<a href="https://www.linkedin.com/"><img src="images/social/linkedin.png" id="linkedin" class="social"></a>
					</div>
				</div>
			</div>
			<div id="footerdesc">
			<a  href="about.php" style="display: block;"> 
				<img class="social" src="images/social/info.png">
			</a>
			Amazetee Website <br> Copyright &copy; <br> Project for Advanced Internet Techonoliges, SEITM master course, Free University of Bolzano
			</div>
	</footer>