<?php 
session_start();
include ("./git/dbconfig.php"); 
include ("functions.php");

if (!isset($_SESSION["email_login"])) {
  ?><script>window.location.href='login.php?log=unlog'; </script><?php

  exit();
}
$receipt;
if(isset($_GET['orderId'])){
	$receipt = $_GET['orderId'];
}

 if(isset($_SESSION['email_login'])){
	$email = $_SESSION['email_login'];
	$user = getUserDataByEmail($email);
	$res = mysqli_fetch_assoc($user);
	//var_dump($res);
	$user_id = $res['id'];
	//var_dump($user_id);
	$query = "SELECT * FROM orders WHERE user_id = ?";
	$owning = getFromDbByValue($query, $user_id);
	$found = false;
	while($check = mysqli_fetch_assoc($owning)){
		if($check['id'] == $receipt){
			$found = true;
		}
	}
	if($found == false){
		?>
		<script>
			alert('You dont have the permission to access to this page');
			window.location.href='profile.php'; 
		</script>
		<?php
		exit();
	}
}
?>


<html>
<head>
	<title>Amazetee | Receipt</title>
	<link rel="stylesheet" type="text/css" href="lib/css/style.css">
	<link rel="stylesheet" type="text/css" href="lib/css/receipt.css">
</head>
<body>
	<div class="wrapper">
		<?php include ("header.php"); ?>
		<div style="text-align: center;">
		<?php if(isset($_SESSION['email_login'])){
			$result = getUserDataByEmail($email);
			$user = mysqli_fetch_assoc($result);
			$query = "SELECT * FROM orders WHERE id = ? LIMIT 1";
			$result = getFromDbByValue($query, $receipt);
			$orderQuery = mysqli_fetch_assoc($result);
		?>
			<div class="receipt">
				<div id="recheader">
					<div id="recname">
					<p>AMAZETEE Inc.</p>
					</div>
					<img id="reclogo" src="images/amazetee.png">
				</div>
				<div id="deliveryinfo">
					<div class="deliveries">
						<p style="font-weight: bold; font-size: 1.1em; ">BUYER</p>
						<p> <?php echo $user['firstname'] ?> <br> <?php echo $user['lastname'] ?> </p>
					</div>
					<div class="deliveries">
						<p style="font-weight: bold; font-size: 1.1em; ">SHIPPING</p>
						<p> <?php echo $orderQuery['meth_delv'] ?> Delivery </p>
					</div>
					<div id="rowdelvbox">
						<div class="deliveries">
							<p style="font-weight: bold; font-size: 1.1em; ">RECEIPT #</p>
							<p style="font-weight: bold; font-size: 1.1em; ">ORDER'S DATE</p>
							<p style="font-weight: bold; font-size: 1.1em; ">DUE DATE</p>

						</div>
						<div class="deliveries">
							<p> <?php echo $orderQuery['id'] ?></p>
							<p><?php echo $orderQuery['date']; ?></p>
							<p><?php echo date('Y-m-d', strtotime($orderQuery['date']. ' + 7 days')); ?></p>
						</div>
					</div>
				</div>

				<div id="itemstable">
					<table id="buyobjs">
						<tr>
							<th>QTY</th>
							<th>DESCRIPTION</th>
							<th>UNIT PRICE</th>
							<th>AMOUNT</th>
						</tr>
						<?php 
						$prodQuery = "SELECT * FROM ordered_prod WHERE order_id = ?";
						$prodReceipt = getFromDbByValue($prodQuery, $receipt);
						while($products = mysqli_fetch_assoc($prodReceipt)){
						?>
						<tr>
							<td><?php echo $products['quant']; ?> </td>
							<td><?php echo $products['type']." ".$products['size']." ".$products['color']; ?> </td>
							<td>$<?php echo $products['price']; ?> </td>
							<td>$<?php echo ($products['price'] *  $products['quant']); ?> </td>
						</tr>
						<?php
						}
						?>
					</table>
					<div>
					<br/>
					<div id="payinfo">
						TOTAL: $<?php echo $orderQuery['tot_price']; ?>
					</div>
					
					</div>
				</div>
				<div id="recfooter">
					<p>Terms & conditions</p>
					<p>Copyright &copy;  2017 </p>				
					
				</div>
			</div>
			<?php
			}
			else {
				?>
				<div>You aren't logged in!</div>
			<?php 
			} ?>

			<div id="prorecomm">
			</div>
		</div>
		<?php include('footer.php'); ?>
	</div>
</body>
</html>