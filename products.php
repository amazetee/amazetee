<?php 
session_start();
include ("./git/dbconfig.php"); 
include ("functions.php");

if(isset($_GET['id'])){
		$post = $_GET['id'];
}
else{
	?><script>window.location.href='404.php'; </script><?php
}

include("cart.php");


?>
<html>
<head>
	<title>Amazetee | Tees and more</title>
	<link rel="stylesheet" type="text/css" href="lib/css/style.css">
	<link rel="stylesheet" type="text/css" href="lib/css/products.css">
	<link rel="stylesheet" type="text/css" href="lib/css/animate.css">
	<script src="lib/js/functions.js"></script>
	<script type="text/javascript">

		function typefunc(myname, num){

			document.getElementById('inptype').value = myname; // use jquery here 			
			document.getElementById('typebox1').style = 'background: lightgrey; border: 2px solid lightgrey;';
			document.getElementById('typebox2').style = 'background: lightgrey; border: 2px solid lightgrey;';
			document.getElementById('typebox3').style = 'background: lightgrey; border: 2px solid lightgrey;';
			document.getElementById('typebox' + num).style = 'background: grey; border: 2px solid grey;';
		}

		function sizefunc(mysize, num){
			
			document.getElementById('inpsize').value = mysize; // use jquery here
			document.getElementById('sizebox1').style = 'background: lightgrey; border: 2px solid lightgrey;';
			document.getElementById('sizebox2').style = 'background: lightgrey; border: 2px solid lightgrey;';
			document.getElementById('sizebox3').style = 'background: lightgrey; border: 2px solid lightgrey;';
			document.getElementById('sizebox4').style = 'background: lightgrey; border: 2px solid lightgrey;';
			document.getElementById('sizebox' + num).style = 'background: grey; border: 2px solid grey;';
		}

		function colorfunc(color, num){

			document.getElementById('inpcolor').value = color; 
			document.getElementById('colbox1').style = 'border: 2px solid lightgrey; opacity: 0.4;';
			document.getElementById('colbox2').style = 'border: 2px solid lightgrey; opacity: 0.4;';
			document.getElementById('colbox3').style = 'border: 2px solid lightgrey; opacity: 0.4;';
			document.getElementById('colbox' + num).style = 'border: 2px solid grey;';

		}

	</script>
</head>
<body>
	<div class="wrapper">
		<?php include ("header.php"); ?>

		<?php
				$query = "SELECT products.*, sales.* FROM products LEFT JOIN sales ON products.id = sales.prod_id WHERE products.id = ?";
				$result = getFromDbByValue($query, $post);
				//var_dump($result);
				$row=mysqli_fetch_assoc($result);
				$prx = calculatePrice($row['p_price'], $row['amount_perc']);
		?>

		<div id="prohead">
			<div id="proimg" style="background-image: url(images/stock/<?php echo $row['p_image'] ?>);">
			</div>

			<div id="prodesc">
				<div class="simbox" id="prodesigner">
					<p class="pname"><?php echo $row['p_name']; ?></p>
					<p class="pdesc">by <span id="des"><?php echo $row['p_designer']; ?></span></p>
					<br/>
					<div style="text-align: left;">
						<form id="cartform" style="display: inline-block;" action="products.php?action=add&id=<?php echo $row['id']; ?>" method="POST">
							<input type="hidden" name="inpname" id="inpname" value="<?php echo $row['p_name']; ?>">
							<input type="hidden" name="inpprice" id="inpprice" value="<?php echo $prx; ?>">
							<input type="hidden" name="inptype" id="inptype">
							<input type="hidden" name="inpsize" id="inpsize">
							<input type="hidden" name="inpcolor" id="inpcolor">
							
							<p>Items in stock: <?=$row['stock']?> </p>
							<p>Quantity: 
							<!-- ANJAN! YOU MIGHT WANT TO USE INPUT SELECT -->
							<?php
							if($row['stock'] > 0){
							?>
							<input type="number" name="inpquant" id="inpquant" onclick="updateQuantity(<?= $prx ?>);checkmax(<?=$row['stock']?>);" style="width: 50px; height: 25px;" value="1" min="1" max="<?=$row['stock']?>">
							<?php
							}
							else{
							?>
							<input type="number" name="inpquant" id="inpquant" onclick="updateQuantity(<?= $prx ?>);checkmax(<?=$row['stock']?>);" style="width: 50px; height: 25px;" value="0" min="1" max="<?=$row['stock']?>" disabled>
							<?php
							}
							?>
							</p>
						</form>
						<span class="pprice" id="tmpprice"><script>updateQuantity(<?php echo $prx ?>)</script> </span>
					</div>
				</div>

				<div class="simbox sm" id="protype"> 
					<div style="display: inline-block; padding: 10px 2px 0px 0px;">Select Type:</div>
					<div class="typebox" id="typebox1" onclick="typefunc('Men', 1)">Men</div>
					<div class="typebox" id="typebox2" onclick="typefunc('Women', 2)">Women</div>
					<div class="typebox" id="typebox3" onclick="typefunc('Kids', 3)">Kids</div>
				</div>


				<div class="simbox sm" id="prosize">
					<div style="display: inline-block; padding: 10px 5px 0px 0px;">Select Size:</div> 
					<div class="sizebox" id="sizebox1" onclick="sizefunc('S', 1)">S</div>
					<div class="sizebox" id="sizebox2" onclick="sizefunc('M', 2)">M</div>
					<div class="sizebox" id="sizebox3" onclick="sizefunc('L', 3)">L</div>
					<div class="sizebox" id="sizebox4" onclick="sizefunc('XL',4)">XL</div>
				</div>

				<div class="simbox sm" id="procolor">
					<div style="display: inline-block; padding: 10px 10px 0px 0px;">Select Color:</div>
					<div class="colbox" id="colbox1" onclick="colorfunc('White',1);"></div>
					<div class="colbox" id="colbox2" onclick="colorfunc('Grey', 2);"></div>
					<div class="colbox" id="colbox3" onclick="colorfunc('Blue', 3);"></div>
				</div>

				<div id="cartmess" class="">
					Please select all preferences!
				</div>

				<div class="simbox" style="margin-top: 30px;">
					<?php
					if($row['stock'] > 0){
					?>
					<div id="procart">Add to cart</div>
					<?php
					}
					else{
					?>
					<div id="procart2" onclick="alert('Sorry! We are out of stock!');">Add to cart</div>
					<?php
					}
					?>
					<div id="probuy" onclick="location.href='orders.php'">Checkout</div>
				
				</div>


			</div>		
		</div>
		<!--recommender-->
		<?php include("recommender.php"); ?>

		<!--footer-->
		<?php include ("footer.php"); ?>

	</div>

	<script type="text/javascript">
		window.addEventListener("DOMContentLoaded", function () {

			var form = document.getElementById("cartform");
			document.getElementById("procart").addEventListener("click", function () {

			var stype = document.getElementById('inptype').value;
			var ssize = document.getElementById('inpsize').value;
			var scolor = document.getElementById('inpcolor').value;

			if( stype != "" && ssize != "" & scolor != ""){
				form.submit();
			}
			else{
				document.getElementById("cartmess").style.display = 'block';
				document.getElementById("cartmess").classList.add("animated", "flash");

				setTimeout(function() {
				  document.getElementById("cartmess").classList.remove("animated", "flash");
				}, 700);
			}	  	
		  	
		});
		});
	</script>

	
</body>