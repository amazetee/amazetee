<?php 
include ("./git/dbconfig.php"); 


function calculatePrice($price, $sale){
	if($sale <= 100 && $sale > 0){
		$newPrice = ($price * $sale) /100;
		return $price - $newPrice;
	}
	else{
		return $price;
	}
}


// SQL QUERIES

function checkUser($email, $password){

	global $con;
	$stmt = $con->prepare("SELECT id FROM users WHERE email=? AND password=?");
	$stmt->bind_param("ss", $email, $password);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();

	return $result;

}

function getUserDataByEmail($email) {

	global $con;
	$stmt = $con->prepare("SELECT * FROM users WHERE email=?");
	$stmt->bind_param('s', $email);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function getUserDataByUsername($username) {

	global $con;
	$stmt = $con->prepare("SELECT * FROM users WHERE username=?");
	$stmt->bind_param('s', $username);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function createNewUserData($email, $firstname, $lastname, $username, $password){

	global $con;
	$stmt = $con->prepare("INSERT INTO `users` (`firstname`, `lastname`, `username`, `email`, `password`) VALUES (?,?,?,?,?)");
	$stmt->bind_param('sssss', $firstname, $lastname, $username, $email, $password);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function updateAddress($email, $address, $city, $pin, $delv){

	global $con;
	$stmt = $con->prepare("UPDATE users SET address=?, city=?, pin=?, delv=? WHERE email =?");
	$stmt->bind_param('sssss', $address, $city, $pin, $delv, $email);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function addCardDetails($email, $cardnum, $cvv, $expm, $expy){

	global $con;
	$stmt = $con->prepare("UPDATE users SET cardnum=?, cvv=?, expm=?, expy=? WHERE email=?");
	$stmt->bind_param('sssss', $cardnum, $cvv, $expm, $expy, $email);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function addOrders($user_id, $prod_quant, $tot_price, $delivery_addr, $meth_delv, $time, $date, $status){

	global $con;
	$stmt = $con->prepare("INSERT into `orders` (`user_id`, `prod_quant`, `tot_price`, `delivery_addr`, `meth_delv`, `time`, `date`, `status`) VALUES (?,?,?,?,?,?,?,?)");
	$stmt->bind_param('ssssssss', $user_id, $prod_quant, $tot_price, $delivery_addr, $meth_delv, $time, $date, $status);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function addOrderedProducts($order_id, $prod_id, $price, $quant, $type, $size, $color, $combination){

	global $con;
	$stmt = $con->prepare("INSERT into ordered_prod (`order_id`, `prod_id`, `price`, `quant`, `type`, `size`, `color`, `combination`) VALUES (?,?,?,?,?,?,?,?)");
	$stmt->bind_param('ssssssss', $order_id, $prod_id, $price, $quant, $type, $size, $color, $combination);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function updateStock($newstock, $prod_id){

	global $con;
	$stmt = $con->prepare("UPDATE products SET stock=? WHERE id=?");
	$stmt->bind_param('ss', $newstock, $prod_id);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}


/* New functions */

function getAllProducts($query){
 	global $con;
	$stmt = $con->prepare($query);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function getFromDbByValue($query, $value){
	global $con;
	$stmt = $con->prepare($query);
	$stmt->bind_param('s', $value);
	$stmt->execute();
	$result = $stmt->get_result();

	$stmt->free_result();
	$stmt->close();
	return $result;
}

function createNewProduct($name, $id, $tags, $price, $image, $designer, $stock){

	global $con;
	$stmt = $con->prepare("INSERT INTO `products` (`p_name`, `p_id`, `tags`,`p_price`, `p_image`, `p_designer`, `stock`) VALUES (?,?,?,?,?,?,?)");
	$stmt->bind_param('ssssssi', $name, $id, $tags, $price, $image, $designer, $stock);
	$stmt->execute();

	$result = false;
	if(mysqli_affected_rows($con) == 1){
		$result = true;
	}

	$stmt->free_result();
	$stmt->close();
	return $result;
}
// function getUserDataByEmail($email) {

// 	global $db;
// 	$stmt = $db->prepare("SELECT * FROM users WHERE email=?");
// 	$stmt->bind_param('s', $email);
// 	$stmt->execute();
// 	$result = $stmt->get_result();

// 	$stmt->free_result();
// 	$stmt->close();
// 	return $result;
// }


?>