<?php 
include ("git/dbconfig.php");
include_once("functions.php");


if(isset($_POST['Imagex'])){

	if(isset($_POST['AdminCode']) && isset($_POST['ProductName']) && isset($_POST['ProductId']) && isset($_POST['tags']) && isset($_POST['Price']) && isset($_POST['Designer']) && isset($_POST['Stock'])){

	if($_POST['AdminCode'] == $adcode){
		$name = $_POST['ProductName'];
		$id = $_POST['ProductId'];
		$tags = $_POST['tags'];
		$price = $_POST['Price'];
		$img = $_FILES['Image']['name'];
		$designer = $_POST['Designer'];
		$stock = $_POST['Stock'];
		$insResult = createNewProduct($name, $id, $tags, $price, $img, $designer, $stock );
		if($insResult){
			?><script>alert("Product inserted correctly!");</script><?php

			// upload picture


			if(isset($_POST['Imagex'])){   

				$file = $_FILES['Image']['name'];
			 	$file_loc = $_FILES['Image']['tmp_name'];
			 	$file_size = $_FILES['Image']['size'];
			 	$file_type = $_FILES['Image']['type'];
			 	
			 	
			 	$folder="images/stock/";


				if(move_uploaded_file($file_loc,$folder.$file))
				{
				  ?>
				  <script>
				  alert('Uploading image file successful!');
				  </script>
				  <?php				  
				}
				else
				{
				  ?>
				  <script>
				  alert('error while uploading image file');
				  </script>
				  <?php
				}
			}
		}
		else {
			?><script>alert("Sorry there has been a problem, the product couldnt be inserted");</script><?php
		}
	}
	else{
		?><script>alert("You don't have the permissions for this operation");</script><?php
	}

	}
	else{
		?><script>alert("Something is missing;");</script><?php
	}
	
}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Administration</title>
	<link rel="stylesheet" type="text/css" href="lib/css/style.css">
	<link rel="stylesheet" type="text/css" href="lib/css/admin.css">
</head>
<body>
	<?php include("header.php"); ?>
	<div id="boxform">
		<div id="formwrapper">
			<form action="" method="POST" enctype="multipart/form-data" autocomplete="off">
				<label for="AdminCode" autocomplete="off">Admin Code</label>
				<input name="AdminCode" type="text" required><br>

				<label for="ProductName">Product Name</label>
				<input name="ProductName" type="text" placeholder="Best TEE" required><br>

				<label for="ProductId">Product ID</label>
				<input name="ProductId" type="text" placeholder="W0001" required><br>
				
				<label for="tags">tags</label>
				<input name="tags" type="text" placeholder="#tag1 #tag2" required><br>
				
				<label for="Price">Price</label>
				<input name="Price" type="number" placeholder="cost..." required><br>
				
				<label for="Image">Image</label>
				<input name="Image" type="file" required><br>
				
				<label for="Designer">Designer</label>
				<input name="Designer" type="text" placeholder="designer" required><br>
				
				<label for="Stock">Stock</label>
				<input name="Stock" type="number" placeholder="50" required><br>
				<button type="submit" name="Imagex">Submit</button><br>
			</form>
		</div>
	</div>
<?php include("footer.php"); ?>
</body>
</html>