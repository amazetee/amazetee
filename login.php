<?php 
session_start();
include ("./git/dbconfig.php"); 
include ("functions.php");

?>

<?php

//LOGIN BLOCK

if(!isset($_SESSION["email_login"])){
	if (isset($_POST["email_login"]) && isset($_POST["password_login"])) {

		$email_login = filter_var($_POST["email_login"], FILTER_SANITIZE_STRING);

		$password_login = $_POST["password_login"];
		$password_login_sha = openssl_digest($password_login, 'sha512');

		$sql = checkUser($email_login, $password_login_sha);
		$userCount = mysqli_num_rows($sql);

		if($userCount == 1) {
			while ($row = mysqli_fetch_array($sql, MYSQLI_ASSOC)) {
				$id = $row["id"];
			}
			
			$_SESSION["email_login"] = $email_login;
			if(isset($_SESSION['active'])){
				?><script>window.location.href='orders.php?log=success';</script>
				<?php
			}
			else{
				?><script>window.location.href='index.php?log=success';</script>
				<?php
			}
		}
		else{		
			?><script>window.location.href='login.php?log=failedlogin'; </script><?php
		}		
	}
}

?>

<?php

// REGISTRATION BLOCK

$reg= @$_POST['reg'];
$fn="";
$ln="";
$un="";
$em="";
$pswd="";
$pswd2="";
$sign_mess ="";

$fn= strip_tags(@$_POST['fname']);
$ln= strip_tags(@$_POST['lname']);
$un= strip_tags(@$_POST['username']);
$em= strip_tags(@$_POST['email']);
$pswd= strip_tags(@$_POST['password']);
$pswd2= strip_tags(@$_POST['password2']);


if ($reg){
	if($em) {

		$em_res = getUserDataByEmail($em);
		$emcheck = mysqli_num_rows($em_res);	

		$em_res2 = getUserDataByUsername($un);
		$emcheck2 = mysqli_num_rows($em_res2);	

		if($emcheck == 0){	

			if($emcheck2 == 0){	
					if($em&&$pswd){			
						if($pswd){					

							if(strlen($fn)>25||strlen($ln)>25){
								$sign_mess = 'input limit is 25 characters';
							}
							else if(strlen($pswd)<8) {
									$sign_mess = 'Your password must be at least 8 characters';
								}					
								else {									

									$email = filter_var($em, FILTER_SANITIZE_EMAIL);
									
									if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
									    $sign_mess="Please enter a valid email address";
									} 

									if ($pswd != $pswd2){
										$sign_mess="Sorry! Passwords do not match!";
									}

									?>
									<div style="display: none;">
									<form id="dateForm" action="" method="POST">
		    						<input id="log-bar" class="login" type="text" name="email_login" size="25" placeholder="Email" value="<?php echo $em; ?>">
		    						<input id="log-bar" class="login" type="password" name="password_login" size="25" placeholder="Password" value="<?php echo $pswd; ?>">
			  						<input id="login-but" type="submit" name="login" value="LOGIN">
			  						</form>
			  						</div>
									<?php

									if($sign_mess==""){

										$pswd = openssl_digest($pswd, 'sha512');
										$query= createNewUserData($em, $fn, $ln, $un, $pswd);

										if($query){}else{printf("Error: The Registration was incomplete! %s\n\n", mysqli_error($con));}
										
										?>
										<script>document.getElementById('dateForm').submit();</script>
										<?php
										}
								}
						}					
					}else{$sign_mess =  'Please fill in all the details!';}

				}else{$sign_mess =  'Account w/ username already exists!';}

		}else{$sign_mess =  'Account w/ email already exists!';}
	}
}

if(isset($_GET['log'])){
	if($_GET['log'] == "unlog"){
		$sign_mess = 'You must log in to continue!';
	}
}

if(isset($_GET['source'])){
	if($_GET['source'] == 'checkout'){
		$sign_mess = 'Please login to complete the purchase';
	}
}

if(isset($_GET['log'])){
	if($_GET['log'] == 'activecart'){
		$_SESSION['active'] = 'active';
	}
}

if(isset($_GET['log'])){
	if($_GET['log'] == 'failedlogin'){
		$sign_mess = "Invalid credentials";
	}
}

?>


<head>
	<title>Amazetee | Registration</title>
	<link rel="stylesheet" type="text/css" href="lib/css/style.css">
	<link rel="stylesheet" type="text/css" href="lib/css/animate.css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

	<script>
		function showreg(){
			document.getElementById('logdiv').style.display = 'none';
			document.getElementById('regdiv').style.display = 'block';
		};

		function showlog(){
			document.getElementById('regdiv').style.display = 'none';
			document.getElementById('logdiv').style.display = 'block';
		};		
	</script>
</head>
<body>
	<div class="wrapper">
		<?php include ("header.php"); ?>
		 
		<div id="loginex">

			<?php if($sign_mess != ""){ ?>
				<div style="padding: 5px; font-size: 12px; border: 1px solid orangered; color: orangered; width: 240px; margin: 10px auto 30px;" class="animated flash">
					<?php echo $sign_mess; ?>
				</div>
			<?php } ?>

			<div id="logdiv">
				<form action="" method="POST">
					<input type="email" name="email_login" placeholder="email"><br/>
					<input type="password" name="password_login" placeholder="password"><br/>
					<input type="submit" name="submit">
				</form>
				<p id="raise">Need an account? <span onclick="showreg()" style="cursor: pointer; color: royalblue;">Signup</span></p>
			</div>

			<div id="regdiv">
				<form action="" method="POST">
					<input type="text" name="fname" placeholder="Firstname" required><br/>
					<input type="text" name="lname" placeholder="Lastname" required><br/>
					<input type="email" name="email" class="email" placeholder="Email" required><br/>
							<div class="email-yes">Hmm, looks like a valid email!</div>
							<div class="email-no">Please enter a valid email</div>
					<input type="text" name="username" placeholder="Username" required><br/>
					<input type="password" name="password" class="password" placeholder="Password" autocomplete="new-password" required><br/>
						    <ul class="helper-text">
						        <li class="length">Must be at least 8 characters long.</li>
						        <li class="lowercase">Must contain a lowercase letter.</li>
						        <li class="uppercase">Must contain an uppercase letter.</li>
						        <li class="special">Must contain a special character.</li>
						        <li class="number">Must contain a number.</li>
						    </ul>
					<input type="password" name="password2" class="password2" placeholder="Re-enter password" required><br/>
							<div class="pass-yes">Yes! Your passwords Match!</div>
							<div class="pass-no">Passwords do not match!</div>
					<input id="reginp" type="submit" disabled="disabled" name="reg">
				</form>
				<p id="raise">Have an account? <span onclick="showlog()" style="cursor: pointer; color: royalblue;">Login</span></p>
			</div>
		</div>
		<div style="height: 10px; padding: 100px 0px;">
		</div>
		<?php include("footer.php"); ?>
	</div>

	<script>

		(function(){

			var email = document.querySelector('.email');
			var password = document.querySelector('.password');
			var password2 = document.querySelector('.password2');

			var helperText = {
			    charLength: document.querySelector('.helper-text .length'),
			    lowercase: document.querySelector('.helper-text .lowercase'),
			    uppercase: document.querySelector('.helper-text .uppercase'),
			    special: document.querySelector('.helper-text .special'),
			    number: document.querySelector('.helper-text .number')
			};

			var pattern = {
		    charLength: function() {
		        if( password.value.length >= 8 ) {
		            return true;
		        }
		    },
		    lowercase: function() {
		        var regex = /^(?=.*[a-z]).+$/;

		        if( regex.test(password.value) ) {
		            return true;
		        }
		    },
		    uppercase: function() {
		        var regex = /^(?=.*[A-Z]).+$/;

		        if( regex.test(password.value) ) {
		            return true;
		        }
		    },
		    special: function() {
		        var regex = /^(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]).+$/;

		        if( regex.test(password.value) ) {
		            return true;
		        }
		    }, 
		    number: function() {
		        var regex = /^(?=.*[0-9]).+$/;

		        if( regex.test(password.value) ) {
		            return true;
		        }
		    } 
			};

			function patternTest(pattern, response) {
			    if(pattern) {
			        addClass(response, 'valid');
			    } else {
			        removeClass(response, 'valid');
			    }
			}

			function addClass(el, className) {
			    if (el.classList) {
			        el.classList.add(className);
			    } else {
			        el.className += ' ' + className;
			    }
			}

			function removeClass(el, className) {
			    if (el.classList) {
			        el.classList.remove(className);
			    } else {
			        el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
			    }
			}

			function hasClass(el, className) {
			    if (el.classList) {
			        console.log(el.classList);
			        return el.classList.contains(className);    
			    } else {
			        new RegExp('(^| )' + className + '( |$)', 'gi').test(el.className); 
			    }
			}

			password.addEventListener('click', function (){
				document.querySelector(".helper-text").style.display = "block";
			});

			password.addEventListener('keyup', function (){
				patternTest( pattern.charLength(), helperText.charLength );
				patternTest( pattern.lowercase(), helperText.lowercase );
				patternTest( pattern.uppercase(), helperText.uppercase );
				patternTest( pattern.special(), helperText.special );
				patternTest( pattern.number(), helperText.number );
				validatesub();
			});



			password2.addEventListener('keyup', function (){
				validatesub();
			});

			email.addEventListener('keyup', function (){
        		validatesub();
		    });

			function validatesub(){

				var regex = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

				if(password.value == password2.value){
					if(password.value != ""){
						document.querySelector(".pass-no").style.display = "none";
						document.querySelector(".pass-yes").style.display = "block";
					}
					else{
						document.querySelector(".pass-no").style.display = "none";
						document.querySelector(".pass-yes").style.display = "none";	
					}

					if( regex.test(email.value) ) {

						document.querySelector(".email-no").style.display = "none";
						document.querySelector(".email-yes").style.display = "block";
					
						if( hasClass(helperText.charLength, 'valid') && 
					         hasClass(helperText.lowercase, 'valid') && 
					         hasClass(helperText.uppercase, 'valid') && 
					         hasClass(helperText.special, 'valid')) {								
							
									$("#reginp").prop( "disabled", false );
									$("#reginp").css("cursor", "pointer");														
						}
					}
					else{
						document.querySelector(".email-yes").style.display = "none";
						document.querySelector(".email-no").style.display = "block";

						$("#reginp").prop( "disabled", true );
						$("#reginp").css("cursor", "not-allowed");
					}

				}
				else{
					document.querySelector(".pass-yes").style.display = "none";
					document.querySelector(".pass-no").style.display = "block";

					$("#reginp").prop( "disabled", true );
					$("#reginp").css("cursor", "not-allowed");
				}
			}			

		})();

	</script>
</body>
