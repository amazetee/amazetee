<?php 

// HEADER FILE - Navigation and Search Bar

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="search.js"></script>
		<div class="headernav">			
			<img id="logo" src="images/amazetee.png" onclick="location.href='index.php'">			
			<div id="navlist">
				<div id="mobilenav">
					<div class="mobimg" onclick="location.href='index.php'">
						<img src="images/social/home.png">
					</div>
					<div class="mobimg" onclick="location.href='profile.php'">
						<img src="images/social/profile.png">
					</div>
					<div class="mobimg" >
						
					<?php if(!isset($_SESSION["email_login"])){ ?>
						<a href="login.php"><img src="images/social/login.png"></a>

						<!-- if user is LOGGED INTO the account (SHOW LOGOUT)-->
					<?php }else if(isset($_SESSION["email_login"])){ ?>
						<a href="logout.php"><img src="images/social/logout.png"></a>
					<?php } ?>	
					</div>
				</div>	
				<div id="stdnav">
					<div class="nav" onclick="location.href='index.php'">HOME</div>
					<div class="nav" onclick="location.href='profile.php'">PROFILE</div>
					<!-- if user is LOGGED OUT of the account (SHOW LOGIN)-->
					<?php if(!isset($_SESSION["email_login"])){ ?>
					<a href="login.php"><div class="nav">LOGIN</div></a>

					<!-- if user is LOGGED INTO the account (SHOW LOGOUT)-->
					<?php }else if(isset($_SESSION["email_login"])){ ?>
					<a href="logout.php"><div class="nav" id="logout">LOGOUT</div></a>
					<?php } ?>	
				</div>
			</div>					
			<div class="nav" onclick="location.href='orders.php'"><img id="cart" src="images/basket.jpeg" ></div>			
		</div>
		<div class="searchbar">
				<input type="text" class="searchinput" placeholder="Search...">
				<div class="result"></div>
		</div>