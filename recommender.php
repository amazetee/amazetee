<?php
include_once("functions.php");
?>

<head>
	<link rel="stylesheet" type="text/css" href="lib/css/orders.css">
</head>

<div class="recommender">
		<p style="font-size: 12px; padding: 10px 0px 20px 0px;">SOME ITEMS YOU MIGHT LIKE</p>
		 			<?php
		 				$recomQuery = "SELECT sales.prod_id, products.p_name, products.p_price, products.p_image, products.p_designer FROM sales LEFT JOIN products ON sales.prod_id = products.id ORDER BY sales.prod_id DESC LIMIT 4";
		 				$result = getAllProducts($recomQuery);
		 				while($row=mysqli_fetch_assoc($result)){
		 					?>
		 
		 					
		 					<div class="productbox" onclick="location.href='products.php?id=<?php echo $row['prod_id'] ?>'">
		 						<div class="pimg" style="background-image: url(images/stock/<?php echo $row['p_image'] ?>);">
		 						</div>
		 						<p class="pname"><?php echo $row['p_name']; ?></p>
		 						<p class="pdesc" style="overflow: hidden;">by <span id="des"><?php echo $row['p_designer']; ?></span></p>
		 					</div>
		 					
		 
		 
		 					<?php 
		 				}
		 			?>
</div>