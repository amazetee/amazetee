<?php 
session_start();
include ("./git/dbconfig.php"); 
include ("functions.php");
include ("cart.php");

$res = false;

if(isset($_POST['addsub'])){

	$address = $_POST['address'];
	$city = $_POST['city'];
	$pin = $_POST['pin'];
	$delv = $_POST['delv'];

	$fulladdress = $address.' '.$city.' '.$pin;

	// get user id by email from session[email login]
	$email = $_SESSION['email_login'];
	$result = updateAddress($email, $address, $city, $pin, $delv);
	$res = true;

}

if(isset($_POST['cardsub'])){

	$cardnum = $_POST['cardnum'];
	$cvv = $_POST['cvv'];
	$expm = $_POST['expm'];
	$expy = $_POST['expy'];
	$tot_price = $_POST['totprice'];
	$prod_quant = 0;
	$subcheck = true;

	$email = $_SESSION['email_login'];
	$cardvalid = false;


	/* Luhn algorithm number checker - (c) 2005-2008 shaman - www.planzero.org *
	 * This code has been released into the public domain, however please      *
	 * give credit to the original author where possible.                      */

	$number = $cardnum;

    // Strip any non-digits (useful for credit card numbers with spaces and hyphens)
    $number=preg_replace('/\D/', '', $number);

    // Set the string length and parity
    $number_length=strlen($number);
    $parity=$number_length % 2;

    // Loop through each digit and do the maths
    $total=0;
    for ($i=0; $i<$number_length; $i++) {
      $digit=$number[$i];
      // Multiply alternate digits by two
      if ($i % 2 == $parity) {
        $digit*=2;
        // If the sum is two digits, add them together (in effect)
        if ($digit > 9) {
          $digit-=9;
        }
      }
      // Total up the digits
      $total+=$digit;
    }

    // If the total mod 10 equals 0, the number is valid
    if($total % 10 == 0){
  	  $cardvalid = true;
    }





	if($cardvalid == true){
		// add card details to user id
		$result = addCardDetails($email, $cardnum, $cvv, $expm, $expy);
	}
	else{
		$subcheck = false;
		echo("<script>alert('This card is invalid! Please try another card.')</script>");
		?><script>window.location.href='orders.php'</script><?php
	}



	$res = getUserDataByEmail($email);
	$row = mysqli_fetch_assoc($res);

	foreach ($_SESSION['shoppingcart'] as $key => $value) {
		if($_SESSION['shoppingcart'][$key]['item_set'] == 'yes'){

			$prod_quant++;

		}
	}


	//insert into orders and ordered_prod
	
	$user_id = $row['id'];
	$prod_quant = $prod_quant;
	$tot_price = $tot_price;
	$delivery_addr = $row['address'].' '.$row['city'].' '.$row['pin'];
	$meth_delv = $row['delv'];
	$time = time();
	date_default_timezone_set("Europe/Rome");
	$date = date('Y-m-d');
	$status = "Ordered"; //Ordered-Shipped-Delivered


	$ins = addOrders($user_id, $prod_quant, $tot_price, $delivery_addr, $meth_delv, $time, $date, $status);

	if($ins){
		$ordQuery = "SELECT * FROM orders WHERE user_id = ? ORDER BY id DESC LIMIT 1";
		$orderRes = getFromDbByValue($ordQuery, $user_id);
		$orderrow = mysqli_fetch_assoc($orderRes);
		$order_id = $orderrow['id'];
		
		foreach ($_SESSION['shoppingcart'] as $key => $value) {
			if($_SESSION['shoppingcart'][$key]['item_set'] == 'yes'){

				$prod_id = $_SESSION['shoppingcart'][$key]['item_id'];
				$price = $_SESSION['shoppingcart'][$key]['item_price'];
				$quant = $_SESSION['shoppingcart'][$key]['item_quantity'];

				$type = $_SESSION['shoppingcart'][$key]['item_type'];
				$size = $_SESSION['shoppingcart'][$key]['item_size'];
				$color = $_SESSION['shoppingcart'][$key]['item_color'];

				$combination = $order_id.$prod_id.$type.$size.$color;

				$insprod = "";
				$insprod = addOrderedProducts($order_id, $prod_id, $price, $quant, $type, $size, $color, $combination);
				
				if($insprod == false){
					$subcheck = false;
				}
				else{
					//reduce stock value (by $quant) in products table where prod_id = prod_id
					$stockQuery = "SELECT * FROM products WHERE id = ?";
					$stockres = getFromDbByValue($stockQuery, $prod_id);
					$stockrow = mysqli_fetch_assoc($stockres);
					$newstock = $stockrow['stock'] - $quant;

					if($newstock < 0){
						$newstock = 0;
					}
					
					$stockque = updateStock($newstock, $prod_id);
				}

			}
		}

	}
	else{
		$subcheck = false;
	}

	if($subcheck){
		unset($_SESSION['shoppingcart']);	
		?><script>window.location.href='receipt.php?orderId=<?php echo $order_id; ?>'</script><?php
	}
	else{
		echo "<script>alert('Something went wrong! Sorry order could not be placed.')</script>";
	}

}
?>

<html>
<head>
	<title>Amazetee | Tees and more</title>
	<link rel="stylesheet" type="text/css" href="lib/css/style.css">
	<link rel="stylesheet" type="text/css" href="lib/css/orders.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script src="lib/js/functions.js"></script>
	<script>
		function showpaymethod(){
			document.getElementById('prepaymethod').style.display = 'none';
			document.getElementById('paymethod').style.display = 'block';
		}

		function handleChange(checkbox, address, city, pin) {
		    if(checkbox.checked == true){
		        document.getElementById("payinpex").value = address;
				document.getElementById("payinpcity").value = city;
				document.getElementById("payinppin").value = pin;
		    }else{
		        document.getElementById("payinpex").value = null;
				document.getElementById("payinpcity").value = null;
				document.getElementById("payinppin").value = null;
		   }
		}

	</script>
</head>

<body>
	<div class="wrapper">

		<?php include("header.php"); ?>
		<div style="margin: 0px 10px 20px 60px; padding-top:20px; color: lightgrey; width: 200px; height: 20px; float: left;">
		My Cart: 
		</div>

		<div id="order">
			
			<div id="products">

			<?php 
				$total = 0;

				if(!empty($_SESSION['shoppingcart'])){
					$total = 0;
					$count = 0;

					foreach ($_SESSION['shoppingcart'] as $key => $value) {

						$newid = $value['item_id'];
						$serial = $value['item_serial'];
						$set = $value['item_set'];

						if($set == "yes"){
						$resQuery = "SELECT products.*, sales.* FROM products LEFT JOIN sales ON products.id = sales.prod_id WHERE products.id = ?";
						$result = getFromDbByValue($resQuery, $newid);
						$row = mysqli_fetch_assoc($result);
						$prx = calculatePrice($row['p_price'], $row['amount_perc']);
						$base_quant = $value['item_quantity'];
						?>

							<div class="ordering">
								<?php ++$count; ?>
								<div class="productdesc">
									<div class="pinfo">
										<h3><?php echo $value['item_name']; ?></h3>
										<h4>By <?php echo $row['p_designer']; ?></h4>
									</div>
						
				<form id="cartform<?php echo $count; ?>" action="orders.php?action=add&id=<?php echo $row['id']; ?>" method="POST">
									<div class="pvalues">
										<div class="number">
											<p class="lab">Stock: <?php echo $row['stock']; ?></p>
											<div class="labin">
											<label for="number">N.</label>

											<!-- ANJAN! YOU MIGHT WANT TO USE INPUT SELECT -->
											<!--- SET MAX = onclick="checkmax()"; -->

											<input type="number" name="inpquant" id="inpquant<?php echo $count; ?>" min="1" max="<?=$row['stock']?>" value="<?php echo $value['item_quantity']; ?>" onchange="updatepr(<?php echo $count; ?>);" style="width:60px; height: 30px;">
											</div>
										</div>	
					
				<div class="details">
					<input type="hidden" name="baseprice" id="baseprice<?php echo $count; ?>" value="<?php echo $prx; ?>">
					<input type="hidden" name="inpname" id="inpname<?php echo $count; ?>" value="<?php echo $value['item_name']; ?>">
					<input type="hidden" name="inpprice" id="inpprice<?php echo $count; ?>" value="<?php echo $value['item_price']; ?>">
					
					
					<input type="text" class="teefeature" name="inptype" id="inptype<?php echo $count; ?>" value="<?php echo $value['item_type']; ?>" readonly>
					<input type="text" class="teefeature" name="inpsize" id="inpsize<?php echo $count; ?>" value="<?php echo $value['item_size']; ?>" readonly>
					<input type="text" class="teefeature" name="inpcolor" id="inpcolor<?php echo $count; ?>" value="<?php echo $value['item_color']; ?>" readonly>
				</form>	
				</div>

									</div>
									<div class="remcart" onclick="location.href='orders.php?action=delete&serial=<?php echo $value['item_serial']?>'">
											Remove from cart
									</div>


								</div>
								<div class="productwrapper" style="">
									<div class="productimg" style="background: url(images/stock/<?php echo $row['p_image']?>); 	background-size: contain; background-position: center; background-repeat: no-repeat; border: 1px solid lightgrey;">
										
									</div>
									<div class="nprice" style="">				
												<p class="pprice" id="price<?php echo $count; ?>">$<?php echo ($prx*$base_quant); $total +=($prx*$base_quant); ?></p>
									</div>
								</div>
							</div>

						<?php

						}
					}

				}

				if($total == 0){ 
					?>
					<div id="nocart">
						You do not have any items in your cart at this moment!
					</div>
					<?php
				}

			?>

			<div id="totalcart">
				Total: $<?php echo $total; ?>
			</div>

					
			</div>

			<div id="payment">
				<div id="payflow">
					<p style="font-family: calibri; font-size: 16px; font-style: italic; float: right; padding: 40px 20px 10px; color: whitesmoke;">
					<span style="font-size: 18px;">&nbsp;&nbsp;</span>PAYMENT</p>
				</div>

				<?php

				if(!isset($_SESSION['email_login'])){

				?>

				<div id="checklog">
					<div>
						Hello! You are not logged in!<br/><br/>
						Please login/signup to complete your purchase.
					</div>
					<div id="orderlog" onclick="window.location='login.php?log=activecart'">
					   Login
					</div>
				</div>

				<?php 
				}else{

					$em = $_SESSION['email_login'];
					$que = getUserDataByEmail($em);
					$row = mysqli_fetch_assoc($que);

					$set_address = ($row['address'] == null)? "4 Privet Drive, England" : $row['address'];
					$set_city = ($row['city'] == null)? "Bolzano" : $row['city'];
					$set_pin = ($row['pin'] == null)? "39100" : $row['pin'];

					if($res == false){
					?>
						<div id="prepaymethod">
							<form action="" method="POST">

							<div class="pline">
								<div class="inpblock" style="width: auto;">
									<div style="float: left; padding-left: 5px;"">SHIPPING ADDRESS:</div>
									<div><input type="text" name="address" id="payinpex" placeholder="<?php echo $set_address; ?>" required></div>							
								</div>
							</div>

							<div class="pline" style="margin-bottom: 5px;">
								<div class="inpblock">
									<div style="float: left; padding-left: 5px;">CITY:</div>
									<div><input type="text" name="city" class="payinp" id="payinpcity" placeholder="<?php echo $set_city; ?>" required></div>
								</div>
								<div class="inpblock">
									<div style="float: left; padding-left: 5px;">PIN:</div>
									<div><input type="number" name="pin" class="payinp" id="payinppin" placeholder="<?php echo $set_pin; ?>" required></div>
								</div>
							</div>	

							<div class="pline" id="blockpline">
								<?php 
								if($row['address'] != null){	
								?>

									&nbsp;&nbsp;
									<div class="adders" id="adder1">
										<input type="checkbox" name="useadd" id="useadd" style="width: 30px;" onclick="handleChange(this, '<?php echo $set_address; ?>', '<?php echo $set_city; ?>', '<?php echo $set_pin; ?>')">
									</div><!--
									--><div class="adders" style="padding-top: 4px;">
										Use stored address
									</div>
								<?php

								}
								else{
									?>
									<div class="adders" id="adder1">
										<input type="checkbox" name="useadd" id="useaddx" style="width: 30px; visibility: hidden">
									</div>
									<div class="adders" id="adder2" style="padding-top: 4px; visibility: hidden;">
										Use stored address
									</div>
									<?php
								}
								?>
								<div class="delv">
									Delivery:
									<select name="delv">
										<option value="Standard">Standard</option>
										<option value="Overnight">Overnight</option>
									</select>
								</div>	
							</div>			
							
							<input type="submit" name="addsub" id="nbutton" style="margin-top: 0px;" value="NEXT">
							</form>
						</div>
					<?php
					}
					else{
					?>
						<div id="paymethod">
							<form action="" method="POST">
							<input type="hidden" name="totprice" value="<?php echo $total; ?>">

							<div class="pline">
								<div class="inpblockx" style="">
									<div style="float: left; padding-left: 5px;">CARD NUMBER:</div>
									<div><input type="number" name="cardnum" class="payinp" id="payinpex2" onKeyPress="if(this.value.length==16) return false;" placeholder="5419190100247488" required></div>
								</div>
								<div class="inpblock" style="width: 75px;">
									<div style="float: left; padding-left: 5px;">CVV:</div>
									<div><input type="number" name="cvv" class="payinp" style="width: 75px;" onKeyPress="if(this.value.length==4) return false;" placeholder="123" required></div>
								</div>
							</div>

							<div class="pline">
								<div class="inpblock">
									<div style="float: left; padding-left: 5px;">EXP MONTH:</div>
									<div><input type="number" min="1" max="12" name="expm" class="payinp" placeholder="04" onKeyPress="if(this.value.length==2) return false;" required></div>
								</div>
								<div class="inpblock">
									<div style="float: left; padding-left: 5px;">EXP YEAR:</div>
									<div><input type="number" min="2017" max="2099" name="expy" class="payinp" placeholder="2025" onKeyPress="if(this.value.length==4) return false;" required></div>
								</div>
							</div>		

							<?php
							if($total > 0){
							?>
								<input type="submit" name="cardsub" id="nbutton" value="PAY">
								<?php
							}
							else{
								?>
								<input type="text" name="cardsub" id="nbutton" value="PAY" onclick="alert('Please add items to your cart!');" readonly>
								<?php
							}
							?>
							</form>
						</div> 

					<?php 
					}
				} 
				?>
			</div>
		</div>
	</div>

	<!--recommender-->
		<?php include("recommender.php"); ?>

	<!--footer-->
		<?php include("footer.php"); ?>
	</div>

	<script type="text/javascript">
		function updatepr(count){
			var element = document.getElementById("inpquant"+count);
			var form = document.getElementById("cartform"+count);

			element.addEventListener("click", function(){				
				var quant = element.value;
				var tot = (document.getElementById("baseprice"+count).value)*quant;
				document.getElementById("price"+count).innerHTML = "$"+tot;

				form.submit();

			});

		}

		(function(){
			console.log('It works!');
			$('#useadd').click();
		})();



	</script>


	
</body>
</html>