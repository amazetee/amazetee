<?php
session_start();
include ("./git/dbconfig.php"); 
include ("functions.php");
?>

<header>
	<title>404 | Amazetee</title>
	<link rel="stylesheet" type="text/css" href="lib/css/style.css">
</header>

<body>
	<?php include('header.php'); ?>

	<div style="margin: 150px auto 200px; width: 90%; padding: 0px 10px; color: grey; text-align: center;">
		Sorry! The page you were looking for could not be found!
	</div>

	<?php include('footer.php'); ?>
</body>
