<?php 
session_start();
include ("./git/dbconfig.php"); 
include ("functions.php");

$status = array('Ordered', 'Shipped', 'Delivered');

?>


<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>
	<link rel="stylesheet" type="text/css" href="lib/css/style.css">
	<link rel="stylesheet" type="text/css" href="lib/css/profile.css">	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>
<div id="wrapper">
	<?php include("header.php"); ?>
	<br/>
	<div id="profileinfo">
	<?php if(!isset($_SESSION['email_login'])){
		?> <div id="checklogx">
					<div>
						Hello! You are not logged in!<br/><br/>
						Please login/signup to view your profile &amp; purchases.
					</div>
					<div id="orderlog" onclick="window.location='login.php?log=activecart'">
					   Login
					</div>
			</div>
				
			<?php
		}
			else {
				$email = $_SESSION['email_login'];
				$result = getUserDataByEmail($email);
				$row = mysqli_fetch_assoc($result);
				?>
				<div id="userimg"> <img id="usrimg" src="images/social/usr.png"></div>
				<div id="usrinfo">
					<div id="username">Welcome, <?php echo $row['username']; ?></div>
					<div id="usercity" onclick="location='https://www.google.it/maps/place/<?php echo $row['city']?>'">City: <?php echo $row['city']; ?></div>
				</div>
		<?php	}
 ?>
	</div>
	<div id="tablewrapper">
		
<?php if(isset($_SESSION['email_login'])){
		if(isset($email)){
		$query = "SELECT orders.* FROM orders RIGHT JOIN users ON orders.user_id = users.id WHERE users.email = ?";
		$orderres = getFromDbByValue($query, $email);
		if($orderres != NULL){
			?>
			<table id="userorders">
				<thead>
					<tr>	
						<th>order</th>
						<th>date</th>
						<th>status</th>
						<th>total</th>
					<th></th>
					</tr>
				</thead>
				<tbody>
				<?php
					while($orders = mysqli_fetch_assoc($orderres)){
						?>
					<tr>
						<td><?php echo $orders['id'];?></td>
						<td><?php echo $orders['date'];?></td>
						<td><?php echo $status[array_search($orders['status'], $status)];?></td>
						<td><?php echo $orders['tot_price'];?> $</td>
						<td> <div class="receiptlnk" onclick="location='receipt.php?orderId=<?=$orders['id']?>'">VIEW</div></td>
					</tr>
				
			<?php	}	?>
				</tbody>
			</table>
			<?php	
			}
			else { ?>
				<div id="noorders">
					<div> You haven't bought our cool products yet, they are waiting for you!</div>
					<div class="receiptlnk" onclick="location='index.php'">BUY</div>
				</div>
				<?php
			}
		}
	  }	?>

	</div>
	<?php include("recommender.php"); ?>
</div>
<?php include("footer.php"); ?>
</body>
</html>