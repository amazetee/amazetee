-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 22, 2017 at 07:49 PM
-- Server version: 5.7.16
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amaze_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `designers`
--

CREATE TABLE `designers` (
  `id` int(11) UNSIGNED NOT NULL,
  `designername` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designers`
--

INSERT INTO `designers` (`id`, `designername`, `email`, `password`, `photo`) VALUES
(1, 'lazyfoxx', 'anjan8@gmail.com', 'azyfoxx1234', ''),
(2, 'AbyssWatcher', 'gabriele.minneci@gmail.com', 'amazing', ''),
(3, 'Antonidas', 'mail@example.com', '12345678', '');

-- --------------------------------------------------------

--
-- Table structure for table `ordered_prod`
--

CREATE TABLE `ordered_prod` (
  `order_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `quant` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `size` varchar(50) NOT NULL,
  `color` varchar(50) NOT NULL,
  `combination` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='products linked to the orders';

--
-- Dumping data for table `ordered_prod`
--

INSERT INTO `ordered_prod` (`order_id`, `prod_id`, `price`, `quant`, `type`, `size`, `color`, `combination`) VALUES
(5, 2, 9, '2', 'Men', 'M', 'White', '52MenMWhite'),
(14, 2, 9, '2', 'Men', 'M', 'White', '142MenMWhite'),
(14, 2, 9, '1', 'Men', 'M', 'Grey', '142MenMGrey'),
(14, 5, 7, '1', 'Men', 'S', 'White', '145MenSWhite'),
(15, 2, 9, '2', 'Men', 'M', 'White', '152MenMWhite'),
(15, 2, 9, '1', 'Men', 'M', 'Grey', '152MenMGrey'),
(15, 5, 7, '1', 'Men', 'S', 'White', '155MenSWhite'),
(16, 2, 9, '3', 'Men', 'M', 'White', '162MenMWhite'),
(16, 5, 7, '1', 'Men', 'M', 'Blue', '165MenMBlue'),
(17, 5, 7, '1', 'Men', 'S', 'White', '175MenSWhite'),
(18, 2, 9, '1', 'Women', 'M', 'Grey', '182WomenMGrey'),
(19, 5, 7, '1', 'Men', 'S', 'White', '195MenSWhite'),
(20, 2, 9, '1', 'Men', 'S', 'White', '202MenSWhite'),
(21, 2, 9, '1', 'Men', 'S', 'White', '212MenSWhite'),
(22, 2, 9, '1', 'Men', 'S', 'White', '222MenSWhite'),
(23, 2, 9, '1', 'Men', 'S', 'White', '232MenSWhite'),
(24, 2, 9, '1', 'Men', 'S', 'White', '242MenSWhite'),
(25, 2, 9, '1', 'Men', 'S', 'White', '252MenSWhite'),
(26, 2, 9, '1', 'Men', 'S', 'White', '262MenSWhite'),
(27, 2, 9, '1', 'Men', 'S', 'White', '272MenSWhite'),
(27, 5, 7, '5', 'Women', 'S', 'White', '275WomenSWhite'),
(28, 5, 7, '7', 'Men', 'M', 'White', '285MenMWhite'),
(28, 2, 9, '5', 'Men', 'S', 'White', '282MenSWhite'),
(29, 5, 7, '7', 'Men', 'M', 'White', '295MenMWhite'),
(29, 2, 9, '5', 'Men', 'S', 'White', '292MenSWhite'),
(30, 5, 7, '7', 'Men', 'M', 'White', '305MenMWhite'),
(30, 2, 9, '5', 'Men', 'S', 'White', '302MenSWhite'),
(31, 5, 7, '7', 'Men', 'M', 'White', '315MenMWhite'),
(31, 2, 9, '5', 'Men', 'S', 'White', '312MenSWhite'),
(32, 5, 7, '7', 'Men', 'M', 'White', '325MenMWhite'),
(32, 2, 9, '5', 'Men', 'S', 'White', '322MenSWhite'),
(33, 5, 7, '7', 'Men', 'M', 'White', '335MenMWhite'),
(33, 2, 9, '5', 'Men', 'S', 'White', '332MenSWhite'),
(40, 5, 7, '7', 'Men', 'M', 'White', '405MenMWhite'),
(40, 2, 9, '5', 'Men', 'S', 'White', '402MenSWhite'),
(40, 5, 7, '1', 'Men', 'S', 'Blue', '405MenSBlue'),
(41, 2, 9, '3', 'Men', 'M', 'White', '412MenMWhite'),
(41, 5, 7, '5', 'Men', 'L', 'Blue', '415MenLBlue'),
(42, 18, 10, '21', 'Men', 'S', 'White', '4218MenSWhite'),
(42, 5, 7, '16', 'Kids', 'XL', 'Blue', '425KidsXLBlue'),
(43, 2, 9, '1', 'Men', 'S', 'White', '432MenSWhite'),
(44, 2, 9, '1', 'Men', 'S', 'White', '442MenSWhite'),
(45, 2, 9, '1', 'Men', 'S', 'White', '452MenSWhite'),
(46, 2, 9, '1', 'Men', 'S', 'White', '462MenSWhite'),
(47, 2, 9, '1', 'Men', 'S', 'White', '472MenSWhite'),
(48, 2, 9, '1', 'Men', 'S', 'White', '482MenSWhite'),
(49, 1, 9, '1', 'Men', 'M', 'White', '491MenMWhite'),
(50, 2, 9, '1', 'Men', 'S', 'White', '502MenSWhite'),
(51, 2, 9, '1', 'Men', 'S', 'White', '512MenSWhite'),
(52, 2, 9, '1', 'Men', 'S', 'White', '522MenSWhite'),
(53, 5, 7, '1', 'Men', 'S', 'White', '535MenSWhite'),
(54, 2, 9, '3', 'Men', 'M', 'White', '542MenMWhite'),
(55, 1, 9, '1', 'Men', 'S', 'White', '551MenSWhite'),
(55, 1, 9, '3', 'Kids', 'XL', 'Blue', '551KidsXLBlue'),
(55, 5, 7, '23', 'Kids', 'XL', 'Blue', '555KidsXLBlue'),
(56, 1, 9, '1', 'Men', 'S', 'White', '561MenSWhite'),
(56, 1, 9, '3', 'Kids', 'XL', 'Blue', '561KidsXLBlue'),
(56, 5, 7, '23', 'Kids', 'XL', 'Blue', '565KidsXLBlue'),
(57, 1, 9, '1', 'Men', 'S', 'White', '571MenSWhite');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `prod_quant` varchar(100) NOT NULL,
  `tot_price` int(11) NOT NULL,
  `delivery_addr` text NOT NULL,
  `meth_delv` varchar(50) DEFAULT NULL,
  `time` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `prod_quant`, `tot_price`, `delivery_addr`, `meth_delv`, `time`, `date`, `status`) VALUES
(1, 1, '2', 27, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(2, 1, '2', 27, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(3, 1, '2', 27, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(4, 1, '2', 27, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(5, 1, '2', 27, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(6, 1, '2', 27, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(7, 1, '2', 27, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(8, 1, '2', 27, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(9, 1, '2', 27, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(10, 1, '3', 34, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(11, 1, '3', 34, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(12, 1, '3', 34, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(13, 1, '3', 34, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(14, 1, '3', 34, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(15, 1, '3', 34, '7 via Carducci Bolzano 39100', NULL, NULL, NULL, 'Ordered'),
(16, 1, '2', 18, '7 via Carducci Bolzano 39100', NULL, '1495036127', '', 'Ordered'),
(17, 1, '2', 14, '7 via Carducci Bolzano 39100', NULL, '1495036204', '', 'Ordered'),
(18, 1, '1', 7, '7 via Carducci Bolzano 39100', NULL, '1495036544', '17', 'Ordered'),
(19, 4, '1', 9, 'a a a', NULL, '1495042439', '2017-05-17', 'Ordered'),
(20, 4, '1', 9, 'q q q', NULL, '1495181037', '2017-05-19', 'Ordered'),
(21, 4, '1', 9, 'aadas asd aca', NULL, '1495195021', '2017-05-19', 'Ordered'),
(22, 4, '0', 0, 'aadas asd aca', NULL, '1495195042', '2017-05-19', 'Ordered'),
(23, 4, '1', 9, 'ad wd dds', NULL, '1495204324', '2017-05-19', 'Ordered'),
(24, 4, '1', 9, 'ad wd dds', NULL, '1495204819', '2017-05-19', 'Ordered'),
(25, 4, '2', 18, 'ad wd dds', NULL, '1495205179', '2017-05-19', 'Ordered'),
(26, 4, '3', 34, 'ad wd dds', NULL, '1495206246', '2017-05-19', 'Ordered'),
(27, 4, '0', 0, 'ad wd dds', NULL, '1495208668', '2017-05-19', 'Ordered'),
(28, 4, '1', 9, 'ad wd dds', '', '1495209995', '2017-05-19', 'Ordered'),
(29, 4, '1', 9, 'Anjan St Bolzano 12345', 'Standard', '1495210136', '2017-05-19', 'Ordered'),
(30, 4, '1', 9, 'Anjan St Bolzano 12399', 'Overnight', '1495210386', '2017-05-19', 'Ordered'),
(31, 4, '1', 9, 'Anjan St Bolzano 12399', 'Standard', '1495222790', '2017-05-19', 'Ordered'),
(32, 4, '1', 9, 'Anjan St Bolzano 12399', 'Standard', '1495222818', '2017-05-19', 'Ordered'),
(33, 4, '1', 9, 'Anjan St Bolzano 12399', 'Standard', '1495222887', '2017-05-19', 'Ordered'),
(34, 4, '1', 9, 'Anjan St Bolzano 12399', 'Standard', '1495223175', '2017-05-19', 'Ordered'),
(35, 4, '1', 9, 'Anjan St Bolzano 12399', 'Standard', '1495223319', '2017-05-19', 'Ordered'),
(36, 4, '1', 9, 'Anjan St Bolzano 12399', 'Standard', '1495223464', '2017-05-19', 'Ordered'),
(37, 4, '1', 18, 'Anjan St Bolzano 12399', 'Standard', '1495223599', '2017-05-19', 'Ordered'),
(38, 4, '1', 7, 'Anjan St Bolzano 12399', 'Standard', '1495223868', '2017-05-19', 'Ordered'),
(39, 4, '1', 7, 'Anjan St Bolzano 12399', 'Overnight', '1495275671', '2017-05-20', 'Ordered'),
(40, 3, '3', 101, 'Corso Libertà 8 Bolzano 12453', 'Overnight', '1495286470', '2017-05-20', 'Ordered'),
(41, 3, '2', 62, 'Corso Libertà 8 Bolzano 12453', 'Standard', '1495286811', '2017-05-20', 'Ordered'),
(42, 6, '2', 322, '1 Kalamandir Kolkata 700010', 'Standard', '1495384047', '2017-05-21', 'Ordered'),
(43, 4, '1', 9, '7 via Carducci Bolzano 39100', 'Standard', '1495386324', '2017-05-21', 'Ordered'),
(44, 4, '1', 9, '7 via Carducci Bolzano 39100', 'Standard', '1495386435', '2017-05-21', 'Ordered'),
(45, 4, '1', 9, '7 via Carducci Bolzano 39100', 'Standard', '1495386469', '2017-05-21', 'Ordered'),
(46, 4, '1', 9, '7 via Carducci Bolzano 39100', 'Standard', '1495386555', '2017-05-21', 'Ordered'),
(47, 4, '1', 9, '7 via Carducci Bolzano 39100', 'Standard', '1495386635', '2017-05-21', 'Ordered'),
(48, 4, '1', 9, '7 via Carducci Bolzano 39100', 'Standard', '1495387245', '2017-05-21', 'Ordered'),
(49, 4, '1', 9, '7 via Carducci Bolzano 39100', 'Standard', '1495387309', '2017-05-21', 'Ordered'),
(50, 7, '1', 9, '4 Privet Drive London 12839', 'Overnight', '1495389500', '2017-05-21', 'Ordered'),
(51, 7, '1', 9, '4 Privet Drive London 12839', 'Standard', '1495389687', '2017-05-21', 'Ordered'),
(52, 7, '1', 9, '4 Privet Drive London 12839', 'Standard', '1495389784', '2017-05-21', 'Ordered'),
(53, 4, '1', 7, '1 Mukherjee Rd Kolkata 700010', 'Standard', '1495447514', '2017-05-22', 'Ordered'),
(54, 4, '1', 27, '1 Mukherjee Rd Kolkata 700010', 'Standard', '1495468162', '2017-05-22', 'Ordered'),
(55, 4, '3', 197, '7 via Carducci Bolzano 39100', 'Standard', '1495474002', '2017-05-22', 'Ordered'),
(56, 4, '3', 197, '7 via Carducci Bolzano 39100', 'Standard', '1495474039', '2017-05-22', 'Ordered'),
(57, 4, '1', 9, '7 via Carducci Bolzano 39100', 'Standard', '1495474297', '2017-05-22', 'Ordered');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `p_name` varchar(100) DEFAULT NULL,
  `p_id` varchar(100) DEFAULT NULL,
  `tags` text NOT NULL,
  `p_price` varchar(100) DEFAULT NULL,
  `p_image` varchar(100) DEFAULT NULL,
  `p_designer` varchar(100) DEFAULT NULL,
  `stock` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `p_name`, `p_id`, `tags`, `p_price`, `p_image`, `p_designer`, `stock`) VALUES
(1, 'Plain Anchor', 'w0001', '#anchor #white', '10', 'white01.png', 'lazyfoxx', 40),
(2, 'Aloning', 'w0002', '#alone #white', '10', 'aloning.png', 'lazyfoxx', 13),
(5, 'Music tone', 'b0001', '#music #black', '10', 'black01.png', 'lazyfoxx', 0),
(6, 'Beast', 'b0001', '#beast #blue', '12', 'blue01.png', 'lazyfoxx', 21),
(9, 'Senior year', 'y0001', '#yellow #highschool', '10', 'yellow01.png', 'lazyfoxx', 32),
(10, 'Gecko', 'b0002', '', '10', 'x1.png', 'lazyfoxx', 15),
(11, '#movbtch', 'g0001', '', '12', 'x2.png', 'lazyfoxx', 32),
(12, 'Cub', 'o0001', '', '10', 'x3.png', 'lazyfoxx', 4),
(13, 'Lion', 'r0001', '', '10', 'x4.png', 'lazyfoxx', 54),
(14, 'Paws', 'w0003', '', '10', 'x5.png', 'lazyfoxx', 12),
(15, 'Pirate', 'w0004', '', '10', 'x6.png', 'lazyfoxx', 4),
(16, 'Clean Circle', 'o0002', '', '12', 'x7.png', 'AbyssWatcher', 5),
(17, 'Dispatch', 'o0003', '', '10', 'x8.png', 'Antonidas', 10),
(18, 'Football', 'gray0001', '', '10', 'x9.png', 'lazyfoxx', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prod_owners`
--

CREATE TABLE `prod_owners` (
  `designer` int(11) NOT NULL,
  `product` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='product-designer relashionship';

--
-- Dumping data for table `prod_owners`
--

INSERT INTO `prod_owners` (`designer`, `product`) VALUES
(1, 1),
(1, 2),
(2, 5),
(3, 6),
(3, 9);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `prod_id` int(11) NOT NULL,
  `amount_perc` int(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='products on sale';

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`prod_id`, `amount_perc`) VALUES
(2, 10),
(5, 30),
(9, 20),
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(50000) NOT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `pin` varchar(50) DEFAULT NULL,
  `delv` varchar(50) DEFAULT NULL,
  `cardnum` varchar(100) DEFAULT NULL,
  `cvv` varchar(10) DEFAULT NULL,
  `expm` varchar(10) DEFAULT NULL,
  `expy` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `email`, `password`, `address`, `city`, `pin`, `delv`, `cardnum`, `cvv`, `expm`, `expy`) VALUES
(4, 'Anjan', 'Karmakar', 'itraveller', 'anjan8@gmail.com', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', '7 via Carducci', 'Bolzano', '39100', 'Standard', '5419190100247488', '121', '12', '2098'),
(5, 'Anjan', 'Karmakar', 'lazyfoxx', 'anjan.ms@live.com', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Anjan', 'Karmakar', 'new', 'new@mail.com', '67a3c0e45d534e1cfd10979ac4931604cc6585b4ceb621b9900b9a0d26b5fa5ec59038d69aa08207167165ac0d9de10d8717bedb29057dc85c63934277b817d2', '1 Kalamandir', 'Kolkata', '700010', 'Standard', '1', '1', '1', '1'),
(17, 'Anjan', 'Karmakar', 'asasasasasasasa', 'a@sds.xom', 'a31bed4060114f2fd2837aa3300550483376b061272da79e80971ee2f515bb705d4868b8081aa5cd0ae947b7bd0a806be3b0c42f5380962e1ec14e87cd2a1d39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `designers`
--
ALTER TABLE `designers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `designername` (`designername`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `ordered_prod`
--
ALTER TABLE `ordered_prod`
  ADD PRIMARY KEY (`combination`),
  ADD UNIQUE KEY `combination` (`combination`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prod_owners`
--
ALTER TABLE `prod_owners`
  ADD PRIMARY KEY (`designer`,`product`),
  ADD UNIQUE KEY `product` (`product`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`prod_id`),
  ADD UNIQUE KEY `prod_id` (`prod_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `email_2` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `designers`
--
ALTER TABLE `designers`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
