<?php
include ("./git/dbconfig.php");
include("functions.php");
// Escape user inputs for security

$term = $_GET['term'];
    // Attempt select query execution
    $query = "SELECT * FROM products WHERE p_name LIKE ?";
    $matching = '%' .$term. '%';
    if($result = getFromDbByValue($query, $matching)){
        if(mysqli_num_rows($result) > 0){
            while($row = mysqli_fetch_array($result)){ ?> 
            <div class="resultobj" onclick="location.href='products.php?id=<?=$row["id"] ?>'">
                <div class="matchbox">
                    <div class="matchname"><?=$row["p_name"]?></div>
                    <div class="matchdesigner"> by <?=$row["p_designer"]?></div>
                </div>
                <img class="searchimg" src="images/stock/<?=$row['p_image']?>">
            </div><?php ;
            }
            // Close result set
            //mysqli_free_result($result);
        } 
        else{
            echo "<p style='padding: 8px 0px;'>No matches found</p>";
        }
      } 
    else{
        echo "<p style='padding: 8px 0px;'> No matches FOUND </p>" ."<script>console.log( 'Debug Objects: " . mysqli_error($con). "' );</script>" ;
    }


function getValuesFromDB($query, $bind){
    global $con;
    $stmt = $con->prepare($query);
    // 'i' means that the bind is an integer->if needs string use 's'
    $stmt->bind_param('s', $bind);
    $stmt->execute();
    $result = $stmt->get_result();
    //$paramList = $result->fetch_array(MYSQLI_ASSOC); //can be put a guard to check that $paramList has only one line
    $paramList = array();
    $i = 0;
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $paramList[$i++] = $row;
        //var_dump($row);
            }
    //echo "<br> PARAMLIST: <br>";
    //var_dump($paramList);
    //close connection
    $stmt->free_result();
    $stmt->close();
    return $paramList;
}

function liveSearch($query, $value){
    global $con;
    $stmt = $con->prepare($query);
    var_dump($stmt);
    $stmt->bind_param('s', $value);
    $stmt->execute();
    $result = $stmt->get_result();

    $stmt->free_result();
    $stmt->close();
    return $result;
}
?>
