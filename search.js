
$(document).ready(function(){
    $('.searchinput').keyup(showResult);
    // Set search input value on click of result item
    clearSearch();
    $('.searchinput').click(showResult);
});


function showResult(){
	/* Get input value on change */
        var inputVal = $(this).val();
        var column = "p_name";
        console.log("val: " + inputVal);
        var resultDropdown = $(this).siblings(".result");
        if(inputVal.length){
            $.get("livesearch.php", {term: inputVal}, {col: column}).done(function(data){
                // Display the returned data in browser
                //console.log(data);
                resultDropdown.html(data);
            });
            console.log(inputVal + " is processed");
        } else{
            resultDropdown.empty();
        }
        $('searchinput').css('border-bottom', 'none');
        $('.result').css('border', '1px solid darkgrey');
    	$('.result').css('border-top', 'none');
}

function clearSearch(){
	$(document).on("click", ".result div", function(){
        $(this).parent(".result").empty();
        $('.result').css('border', 'none');
    });
    $(document).click(function(){
    	$('.result').empty();
        $('.result').css('border', 'none');
    });
}
