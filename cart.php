<?php
/*
 The key to this problem lies in assigning an item_serial which corresponds to the user selection >

 item id = 9
 type = m
 size = s
 color = b

 then, in this case our serial will be = 9msb


*/

if (isset($_GET['action'])) {



	if($_GET['action'] == 'add'){

		if(isset($_POST['inptype'])){

		$serial = $_GET['id'].$_POST['inptype'].$_POST['inpsize'].$_POST['inpcolor'];
		$count  = 0;

		if(isset($_SESSION['shoppingcart'])){		
			$item_array_serial = array_column($_SESSION['shoppingcart'], 'item_serial');
			$item_array_set = array_column($_SESSION['shoppingcart'], 'item_set');


			if(!in_array($serial, $item_array_serial)){	

				
				// if item id NOT INSIDE array PUSH details
				//echo "<script>alert('NOT INSIDE ARRAY');</script>";

				$item_array = array(
					'item_set' => "yes",
					'item_serial' => $serial,
					'item_id' => $_GET['id'], 
					'item_name' => $_POST['inpname'],
					'item_type' => $_POST['inptype'],
					'item_size' => $_POST['inpsize'],
					'item_color' => $_POST['inpcolor'],
					'item_price' => $_POST['inpprice'],
					'item_quantity' => $_POST['inpquant']
				);

				$count = ++$_SESSION['count'];
				$_SESSION['shoppingcart'][$count] = $item_array;
				echo "<script>alert('ITEM ADDED TO CART!');</script>";

			}
			else{

				// if item id IS INSIDE array //UPDATE CART is NEW VALUES			
				$item_array = array(
					'item_set' => "yes",
					'item_serial' => $serial,
					'item_id' => $_GET['id'], 
					'item_name' => $_POST['inpname'],
					'item_type' => $_POST['inptype'],
					'item_size' => $_POST['inpsize'],
					'item_color' => $_POST['inpcolor'],
					'item_price' => $_POST['inpprice'],
					'item_quantity' => $_POST['inpquant']
				);

				if (in_array($serial, $item_array_serial)) {	

					foreach ($item_array_serial as $keys => $value) {
						if($value == $serial){
							$savekey = $keys;
						}
					}
					$key = $savekey;
					$setvalue = $_SESSION['shoppingcart'][$key]['item_set'];

					//echo $key;
					//echo "<script>alert('IN INSIDE ARRAY - DETECTED KEY');</script>";

					if($setvalue == "yes"){

						//echo "<script>alert('IN INSIDE ARRAY - SETVALUE YES');</script>";
					   	if($_SESSION['shoppingcart'][$key]['item_serial'] == $item_array['item_serial']){

							if($_SESSION['shoppingcart'][$key]['item_quantity'] == $item_array['item_quantity']){

								echo "<script>alert('ITEM ALREADY IN CART!');</script>";
							}
							else{
								$_SESSION['shoppingcart'][$key] = $item_array;
						   		echo "<script>alert('CART ITEM UPDATED!');</script>";
							}
					   	}
					   	else{
					   		$count = ++$_SESSION['count'];
							$_SESSION['shoppingcart'][$count] = $item_array;
							echo "<script>alert('ITEM ADDED TO CART!');</script>";
					   	}
					}  	
					else{
						//echo "<script>alert('IN INSIDE ARRAY - SETVALUE No');</script>";
					   	$count = ++$_SESSION['count'];
						$_SESSION['shoppingcart'][$count] = $item_array;
						echo "<script>alert('ITEM ADDED TO CART!');</script>";
					}
				} 	
			}
		}
		else{

			$item_array = array(
				'item_set' => "yes",
				'item_serial' => $serial,
				'item_id' => $_GET['id'], 
				'item_name' => $_POST['inpname'],
				'item_type' => $_POST['inptype'],
				'item_size' => $_POST['inpsize'],
				'item_color' => $_POST['inpcolor'],
				'item_price' => $_POST['inpprice'],
				'item_quantity' => $_POST['inpquant']
			);

			$_SESSION['count'] = 0;
			$count = $_SESSION['count'];

			$_SESSION['shoppingcart'][$count] = $item_array;
			echo "<script>alert('ITEM ADDED TO CART!');</script>";
		}

	// --------------------------------------------------------------------------------------------------------------
		}
	}
	else if($_GET['action'] == 'delete'){

		foreach ($_SESSION['shoppingcart'] as $key => $value) {
			
			if($value['item_serial'] == $_GET['serial']){
				$_SESSION['shoppingcart'][$key]['item_set'] = "no";
				echo "<script>alert('ITEM REMOVED!');</script>";
				echo "<script>window.location='orders.php'</script>";
			}
		}
	}
}

?>