<?php 
	session_start();
	include ("./git/dbconfig.php");
	include_once("functions.php"); 
?>
<html>
<head>
	<title>Amazetee | Tees and more</title>
	<link rel="stylesheet" type="text/css" href="lib/css/style.css">
	<link rel="stylesheet" type="text/css" href="lib/css/animate.css">
</head>
<body>
	<div class="wrapper">
		<?php include ("header.php"); ?>

		
		<div id="onsale">
			<p style="padding: 20px 0px;">ITEMS ON SALE TODAY!</p>
			
			<?php 
				$query = "SELECT sales.prod_id, sales.amount_perc, products.p_name, products.p_price, products.p_image, products.p_designer FROM sales LEFT JOIN products ON sales.prod_id = products.id ORDER BY sales.prod_id DESC LIMIT 4";
				$salesQuery = getAllProducts($query);
				while ($sale = mysqli_fetch_assoc($salesQuery)){
					//var_dump($sale);
					?>
					<div class="saleitem" onclick="location.href='products.php?id=<?php echo $sale['prod_id'] ?>'">
						<img src="images/sale2.png" class="salead animated tada infinite">
						<div class="saletitle" onclick="location.href='products.php?id=<?php echo $sale['prod_id'] ?>'">
								<div class="teename" ><?=$sale['p_name']?></div>
								<div class="teedesigner">by <?=$sale['p_designer']?></div>
							
						</div>
						<img src="images/stock/<?=$sale['p_image']?>" class="salephoto">
						<div class="saleprice">$ <?=calculatePrice($sale['p_price'], $sale['amount_perc']) ?> </div>
					</div>
					<?php
				}
			?>		
				
		</div>

		<div class="productlist">
			<p style="padding: 20px 0px 40px;">AMAZETEE COLLECTION</p>
			<div style="max-width: 99%;">
			<?php
			$query = "SELECT * FROM products";
				$result = getAllProducts($query);
				while($row=mysqli_fetch_assoc($result)){
					?>

					
					<div class="productbox" onclick="location.href='products.php?id=<?php echo $row['id'] ?>'">
						<div class="pimg" style="background-image: url(images/stock/<?php echo $row['p_image'] ?>);">
						</div>
						<p class="pname"><?php echo $row['p_name']; ?></p>
						<p class="pdesc">by <span id="des"><?php echo $row['p_designer']; ?></span></p>
						<div class="pprice">
							<p id="price">$<?php echo $row['p_price']; ?></p>
						</div>
					</div>
					


					<?php 
				}
			?>
			</div>
			<p style="padding: 20px 0px 40px;"></p>
		</div>
		<?php include ("footer.php"); ?>
	</div>

</body>
</html>
